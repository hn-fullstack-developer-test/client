# Client Component (Angular 8 + Angular Material)

This project contains the Angular client of the HN (Fullstack Developer Test) project.

## CI/CD
It is built with CI/CD functionalities through the .gitlad-ci.yml file located at the root of the project. The configured pipeline includes the stages: install, test and build. This last stage releases a downloadable artifact that contains the client code to be published on any http server that supports SPA-type pages.

## Running local unit tests

To run local tests you must clone the project, run an `npm install`, and then run an `npm test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running local end-to-end tests

Run `npm e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Run Local Client

To lift the angular client run an `npm start`

To check its operation visit the *http://localhost:4200* url in the browser.

## Run Local Client via Docker

A Dockerfile file that allows a multi-stage build was also added, to run the functionality you must have Docker installed on your system, clone the project, cd to it and run the following command to create the image:

`docker image build -t fdt_client .`

And then the following to start it:

`docker container run --publish 80:80 --detach --name client fdt_client`


To check its operation visit the *http://localhost* url in the browser.


Remember that if the API functionality is not active, the Angular client will not be able to access the records.
