import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { DataApiService } from './data-api.service';

describe('DataApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: DataApiService = TestBed.get(DataApiService);
    expect(service).toBeTruthy();
  });
});
