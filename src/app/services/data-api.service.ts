import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "./../../environments/environment";

@Injectable({
  providedIn: "root"
})
export class DataApiService {
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {}

  async getPostsAsync() {

    const data = await this.http.get(this.baseUrl + "posts").toPromise().catch(() => {
      console.log('error in fetching posts');
    });
    
    if (data) {
      return data;
    }

    if (!data) {
      return;
    }

  }
  
  async delPostsAsync(id: string) {
    try {
      await this.http.delete(this.baseUrl + `post/${id}`).toPromise();
    } catch {
      console.log('error in deleting posts');
    }
  }

}
