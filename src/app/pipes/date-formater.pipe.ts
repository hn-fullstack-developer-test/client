import { Pipe, PipeTransform } from "@angular/core";
import * as moment from "moment";

@Pipe({
  name: "dateFormater"
})

export class DateFormaterPipe implements PipeTransform {
  transform(value: any, ...args: any[]): any {

    return moment(value).calendar(null, {
      lastWeek: "MMM DD",
      lastDay: "[Yesterday]",
      sameDay: "h:mm a",
      sameElse: "MMM DD"
    });
  }

}
